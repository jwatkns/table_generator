﻿using System;

namespace TbGen
{
    public class index : Attribute
    {
    }    

    public class db_notnull : Attribute
    {
    }

    public class fk : Attribute
    {
        public string table { get; set; }
    }
    public class col_len : Attribute
    {
        public int len { get; set; }
    }
}
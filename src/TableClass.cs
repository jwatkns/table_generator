﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TbGen
{
    public class TableClass
    {
        private Dictionary<string, db_field> _fields = new Dictionary<string, db_field>();
        private string _className = String.Empty;

        private Dictionary<Type, string> dataMapper
        {
            get
            {
                // Add the rest of your CLR Types to SQL Types mapping here
                Dictionary<Type, String> dataMapper = new Dictionary<Type, string>();
                dataMapper.Add(typeof(long), "BIGINT");
                dataMapper.Add(typeof(int), "INT");
                dataMapper.Add(typeof(string), "VARCHAR({0})");
                dataMapper.Add(typeof(bool), "BIT");
                dataMapper.Add(typeof(DateTime), "DATETIME");
                dataMapper.Add(typeof(float), "FLOAT");
                dataMapper.Add(typeof(decimal), "DECIMAL(18,0)");
                dataMapper.Add(typeof(Guid), "UNIQUEIDENTIFIER");

                return dataMapper;
            }
        }

        public Dictionary<string, db_field> Fields
        {
            get { return this._fields; }
            set { this._fields = value; }
        }

        public string ClassName
        {
            get { return this._className; }
            set { this._className = value; }
        }

        public TableClass(Type t)
        {
            this._className = t.Name;

            foreach (PropertyInfo p in t.GetProperties())
            {
                bool isIndexed = false;
                bool isNotNullable = false;
                bool isFk = false;
                string fk_table = "";
                int len = 100;
                bool isLen = false;
                if (p.CustomAttributes != null && p.CustomAttributes.Count() > 0)
                {
                    isIndexed = p.CustomAttributes.Any(x => x.AttributeType.Name.Equals("index", StringComparison.OrdinalIgnoreCase));
                    isNotNullable = p.CustomAttributes.Any(x => x.AttributeType.Name.Equals("db_notnull", StringComparison.OrdinalIgnoreCase));
                    isFk = p.CustomAttributes.Any(x => x.AttributeType.Name.Equals("fk", StringComparison.OrdinalIgnoreCase));
                    isLen = p.CustomAttributes.Any(x => x.AttributeType.Name.Equals("col_len", StringComparison.OrdinalIgnoreCase));
                    if (isFk)
                    {
                        CustomAttributeData customAttributeData  = p.CustomAttributes.Where(x => x.AttributeType.Name.Equals("fk", StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        fk_table = (string)customAttributeData.NamedArguments.Where(x => x.MemberName.Equals("table", StringComparison.OrdinalIgnoreCase)).FirstOrDefault().TypedValue.Value;
                    }
                    if (isLen)
                    {
                        CustomAttributeData customAttributeData = p.CustomAttributes.Where(x => x.AttributeType.Name.Equals("col_len", StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        len = (int)customAttributeData.NamedArguments.Where(x => x.MemberName.Equals("len", StringComparison.OrdinalIgnoreCase)).FirstOrDefault().TypedValue.Value;                        
                    }
                }
                db_field dbField = new db_field
                {
                    data_type = p.PropertyType,
                    is_indexed = isIndexed,
                    name = p.Name,
                    is_not_nullable = isNotNullable,
                    is_fk = isFk,
                    fk_table = fk_table,
                    len = len
                };

                this.Fields.Add(p.Name, dbField);
            }
        }

        public void GetForeignKeyScript(StringBuilder sqlScript)
        {
            var fields = this.Fields.Where(x => x.Value.is_fk);
            foreach (var kv in fields)
            {
                var field = kv.Value;
                if (field.is_fk)
                {
                    string fkScript = $@"
GO
    ALTER TABLE {this.ClassName} WITH NOCHECK
        ADD CONSTRAINT FK_{this.ClassName}_{field.name} FOREIGN KEY({field.name}) REFERENCES {field.fk_table}(id)
GO";
                    sqlScript.AppendLine($"-- FOREIGN KEY TO {field.fk_table} USING FIELD {this.ClassName}.{field.name}");
                    sqlScript.AppendLine(fkScript);                    
                }
            }

        }

        

        public string CreateTableScript()
        {
            System.Text.StringBuilder script = new StringBuilder();
            string comment = $@"
--**********************************************
--* {this.ClassName}
--**********************************************";
            script.AppendLine(comment);            
            script.AppendLine("CREATE TABLE " + this.ClassName);
            script.AppendLine("(");
            int i = 0;
            foreach (var kv in this.Fields)
            {
                var field = kv.Value;
                if (dataMapper.ContainsKey(field.data_type))
                {
                    if (field.name.Equals("id", StringComparison.OrdinalIgnoreCase))
                    {
                        script.Append($@"   {field.name} {dataMapper[field.data_type]} IDENTITY(1,1) NOT NULL");
                    }
                    else
                    {
                        string notNullString = field.is_not_nullable ? " NOT NULL" : "";
                        string fieldScript = $@"    {field.name} {dataMapper[field.data_type]}{notNullString}";
                        if(field.data_type == typeof(string))
                        {
                            string len = field.len == 0 ? "50" : field.len.ToString();
                            fieldScript = string.Format(fieldScript, len);
                        }
                        script.Append(fieldScript);
                    }

                }
                else
                {
                    // Complex Type? 
                    script.Append("\t " + field.name + " BIGINT");
                }

                if (i != this.Fields.Count - 1)
                {
                    script.Append(",");
                }
                ++i;
                script.Append(Environment.NewLine);
            }
            var indexedFields = this.Fields.Where(x => x.Value.is_indexed);
            script.AppendLine("-- PRIMARY INDEX");
            script.AppendLine($@",
CONSTRAINT [PK_{this.ClassName}] PRIMARY KEY CLUSTERED ([id] ASC) 
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
    ON [PRIMARY]
) ON [PRIMARY]
GO");
            script.Append(Environment.NewLine);
            //get indexes
            foreach (var index in indexedFields)
            {
                var field = index.Value;
                script.AppendLine($"-- INDEX IDX_{this.ClassName}_{field.name}");
                script.AppendLine($@"
CREATE NONCLUSTERED INDEX [IDX_{this.ClassName}_{field.name}] ON [dbo].[{this.ClassName}]
(
	[{field.name}] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO");
                script.Append(Environment.NewLine);
            }
            GetForeignKeyScript(script);
            script.AppendLine($"-- **** END {this.ClassName} ******************************");
            return script.ToString();
        }
    }
}

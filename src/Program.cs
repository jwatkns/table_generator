﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TbGen
{
    class Program
    {
        static void Main(string[] args)
        {
            List<TableClass> tables = new List<TableClass>();
            StringBuilder sqlScript = new StringBuilder();

            
            Assembly a = TbAssemblyLoader.GetAssembly();
            if (a == null) return;

            Type[] types = a.GetTypes();

            // Get Types in the assembly.
            foreach (Type t in types)
            {
                TableClass tc = new TableClass(t);
                tables.Add(tc);
            }
            sqlScript.AppendLine(@"
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO");
            GetDropConstraintsScript(sqlScript, tables);
            GetDropTablesScript(sqlScript, tables);
            // Create SQL for each table
            foreach (TableClass table in tables)
            {
                string tableScript = table.CreateTableScript();
                sqlScript.AppendLine(tableScript);
                Console.WriteLine(tableScript);
                Console.WriteLine();
            }

            // Total Hacked way to find FK relationships! Too lazy to fix right now
            
            File.WriteAllText($@"c:\temp\{a.GetName().Name}.sql", sqlScript.ToString());
        }

        private static void GetDropTablesScript(StringBuilder sqlScript, List<TableClass> tables)
        {
            foreach (TableClass table in tables)
            {
                sqlScript.AppendLine($@"DROP TABLE [dbo].[{table.ClassName}]");
            }
        }

        private static void GetDropConstraintsScript(StringBuilder sqlScript, List<TableClass> tables)
        {

            sqlScript.AppendLine($@"
while(exists(select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE='FOREIGN KEY'))
begin

	declare @sql nvarchar(2000)

	SELECT TOP 1 @sql=('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME

	+ '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')

	FROM information_schema.table_constraints

	WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'

	exec (@sql)

end
");
        }
        
    }    

  
}
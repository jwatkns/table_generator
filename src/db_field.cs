﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TbGen
{
    public class db_field
    {
        public string name { get; set; }
        public Type data_type { get; set; }
        public bool is_indexed { get; set; }
        public bool is_not_nullable { get; set; }
        public bool is_fk { get; set; }
        public string fk_table { get; set; }
        public int len { get; set; }
    }
}
